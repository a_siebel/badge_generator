import miniworldmaker

board = miniworldmaker.PixelBoard((128,128))


class Logo:
    
    def __init__(self, board, title, output_file, logo, font_size):
        self.logo = logo
        self.title = title
        self.output_file = output_file
        self.board = board
        self.font_size = font_size
        self.bg = miniworldmaker.Rectangle((0,0),128,128)
        self.logo = miniworldmaker.Token((32,32))
        self.logo.add_costume(logo)
        self.logo.size = (64, 64)
        self.logo.costume.alpha = 100
        self.bg.border = 10
        self.bg.border_color = (0, 160, 100, 120)
        self.bg.color = (0, 255, 100, 40)
        self.title_text = miniworldmaker.TextToken((16,12))
        self.title_text.costume.font_size = font_size
        self.title_text.auto_size = "token"
        self.title_text.set_text(title)
        
    def silver(self):
        self.bg.border_color = (211, 211, 211, 255)
        self.bg.color = (211, 211, 211, 200)
        print("...silver")
        print(self.board.tokens)
        self.descr_token = miniworldmaker.TextToken((44,102))
        self.descr_token.costume.font_size = 14
        self.descr_token.auto_size = "token"
        self.descr_token.set_text("Silver")
        miniworldmaker.ActionTimer(20, self.screenshot, "_silver.png" )
        
    def screenshot(self, suffix):
        self.board.screenshot("output/" + self.output_file + suffix)
        miniworldmaker.ActionTimer(5, self.descr_token.remove)
        
        
    def gold(self):
        self.bg.border_color = (255, 223, 0, 255)
        self.bg.color = (255, 223, 0, 200)
        print("....gold")
        self.descr_token = miniworldmaker.TextToken((44,102))
        self.descr_token.costume.font_size = 14
        self.descr_token.auto_size = "token"
        self.descr_token.set_text("Gold")
        miniworldmaker.ActionTimer(20, self.screenshot, "_gold.png" )
    

    def platin(self):
        self.bg.border_color = (160, 178, 198, 255)
        self.bg.color = (180, 198, 220, 200)
        print("......platin")
        self.descr_token = miniworldmaker.TextToken((40,102))
        self.descr_token.costume.font_size = 14
        self.descr_token.auto_size = "token"
        self.descr_token.set_text("Platin")
        miniworldmaker.ActionTimer(20, self.screenshot, "_platin.png" )

@board.register
def on_setup(self):
    self.logo = Logo(self, "First Lesson", "first_lesson", "images/database.png", 14)
@board.register
def on_started(self):
    print("started!")
    miniworldmaker.ActionTimer(1, self.logo.silver, None)
    miniworldmaker.ActionTimer(24, self.logo.gold, None)
    miniworldmaker.ActionTimer(48, self.logo.platin, None)
    miniworldmaker.ActionTimer(80, self.quit, None)
        
board.run()
